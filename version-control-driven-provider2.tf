 terraform {
#   cloud {
#     organization = "Krisdorf-project"
#     workspaces {
#       name = "Demo-workspace"
#     }
#   }
  required_version = ">=1.3.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }
  }
}

provider "aws" {
  region     = "us-east-1"
  access_key = ""
  secret_key = ""
}

#9a Create_ubuntu_server
resource "aws_instance" "web-server-instance" {
  ami               =var.ami 
  instance_type     = var.instance_type
  availability_zone = "us-east-1a"
  key_name          = "Novakeypair"
}

variable "instance_type" {
    # type = "string"
    # description = "my ec2 instance type"
    # default = ""
  
}

variable "ami" {
    # type = "string"
    # description = "ami variable"
    # default = ""
  
}